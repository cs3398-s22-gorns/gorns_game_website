# Gorns Website by Team Gorns

> © Team Gorn's: Eastland Crane, Ryan Larsen, Andrew Killian - 2022.

> Live demo [_here_](https://erawebsite.ecrane0107.repl.co).

## Table of Contents

* [General Info](#general-info)
* [Features](#features)
* [Screenshots](#screenshots)
* [Usage](#usage)
* [Contact](#contact)
* [Sprint 2](#sprint-2-contributions)
* [Sprint 3](#sprint-3-contributions)
* [Next Steps](#next-steps)

## General Info

## Features
* Pool Game
* Gorn Jumper
* Gorn Escape (Text Adventure)
* Pong
* Snake

## Contact

- ***Eastland's Email: ecrane0107@gmail.com***
- ***Ryan's Email: ryanlarsen2002@gmail.com***

## Sprint 2 Contributions

### Eastland Contribution 

* Website Creation
* Helped set up pool game W/Ryan Larsen.
* Created Gorn Jumper
* Created Gorn Escape
* Create About us/gorns webpage

### Ryan Contribution

* Heavily updated and improved pool game
* Created Art for Snake Game
* Created Code for Snake Game
* Created webpage for Snake Game

### Andrew Contribution

* Created Pong Game
* Created light/dark theme button
* Improved CSS 

## Sprint 2 Retrospective

### What Went Well?

***Team:***

* Helping/teaching other how to create test cases, and merge the master branch into feature branches
* Showing up to group meetings when we say we’re going to.
* Having one-on-one meetings to discuss code improvement and integration.

***Individual:***

* Andrew – Designing and coding a dark theme mode, as well as creating some test cases for the pool game
* Ryan - Learning JavaScript and creating new code/updating existing code went very smoothly.
* Eastland - Merging went alot more smoother, Learned alot more about team platforms, and coding in general.

### What Can I do to improve?

***Andrew:***

* Time management. I found that this sprint I underestimated the learning curve of learning a testing framework like Jest. I will try and give myself more leeway to learn a new technology that we might incorporate into our project.
* My team and instructor will know that I am doing better by more communication on the slack channel and by not waiting too long into the sprint to start working on tasks and committing.

***Ryan:***

* Completing tasks earlier in the sprint. Since I was sick for the entire second half of the sprint, I wish I would have had more work completed early on.
* Take some time outside of class to learn JavaScript. I learned enough JavaScript over the course of the sprint to be able to complete my tasks, and learning went smoothly. However, in order to make better contributions to the team, I should refine my JavaScript skills on my own time.

***Eastland:***

* I need to learn to resolve merge conflicts better and more effectively.
* I need to measure my skill set in coding better and try to push myself to go after harder tasks/projects.
* Need to measure my time better!

## Sprint 3 Contributions

### Eastland Contribution

* [GPG-36](https://cs3398s22gorns.atlassian.net/browse/GPG-36?atlOrigin=eyJpIjoiNzc5MDFiNGI5OGNjNDVlMzgwYjZlNmI5ZWVjZTk5NGIiLCJwIjoiaiJ9) Gorn Jumper - (https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/54283af5135e3243b2a811169dfc52b055ad898f)
* [GPG-32](https://cs3398s22gorns.atlassian.net/browse/GPG-32?atlOrigin=eyJpIjoiOTkwNzUwOTAyNDcxNDZhNTliYmIxMzc1NDZhMzc5NWQiLCJwIjoiaiJ9) Gorn Escape (Text adventure) - (https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/4d5bafb287e8340f27c22f5878425a8809885466)
* [GPG-34](https://cs3398s22gorns.atlassian.net/browse/GPG-34?atlOrigin=eyJpIjoiZmViZWVmZTkyYTYyNDhkMmIwNDZhMjlhYzFlNTgzODQiLCJwIjoiaiJ9) Website update/managment - (https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/bfcebcfd16c14027ebc88e7cc77cc156fcf2a9f3)

### Ryan Contribution

* [GPG-33](https://cs3398s22gorns.atlassian.net/browse/GPG-33?atlOrigin=eyJpIjoiZGQyZGY0NDcyNDQzNDExODg3YWI1YzQwYjdhM2UxNGIiLCJwIjoiaiJ9): Fixed positioning of credits on main menu for pool game and added indicator for player color. Commits: [Fixed Credits](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/8c6b0d3e18999c51c4277ce274a52a6ef5ea008b), [Color Indicator](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/7344c4603e89b69778dbdd4b0b987435f2cb686d)
* [GPG-31](https://cs3398s22gorns.atlassian.net/browse/GPG-31?atlOrigin=eyJpIjoiZmQ4M2FlY2JhNGRiNDZmYmFmNGNiOWY0ZjIzNmYwNWMiLCJwIjoiaiJ9): Created artwork to use in the snake game, and then transitioned to CSS instead of sprites. Commits: [Sprite Artwork](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/0a53a17aa01f2bd4bb7d64e61e7ebf68b23a0a37), [Switch to CSS](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/a64736d6eb7723afef00dba2135bdddb37d2e97a)
* [GPG-41](https://cs3398s22gorns.atlassian.net/browse/GPG-41?atlOrigin=eyJpIjoiM2EzY2I4ZGYxOTViNDdlYTlhZjRlOTVhNWQ3MDE4ZGUiLCJwIjoiaiJ9): Created an html file for the snake game. Commits: [HTML File](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/c70fa87ccb537845fe91765f6f9af39ed5000af5)
* [GPG-37](https://cs3398s22gorns.atlassian.net/browse/GPG-37?atlOrigin=eyJpIjoiMWI5MTE0NjI5ODliNDIyN2I5MDA5OTMzYjY3ZGYwMTAiLCJwIjoiaiJ9): Added a section on the navbar on the website to navigate to the snake game. Commits: [Navbar](https://cs3398s22gorns.atlassian.net/browse/GPG-37?atlOrigin=eyJpIjoiMWI5MTE0NjI5ODliNDIyN2I5MDA5OTMzYjY3ZGYwMTAiLCJwIjoiaiJ9), [Directory Fix](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/f7998930395d7048afce752ce0e98f7223610ab4)
* [GPG-44](https://cs3398s22gorns.atlassian.net/browse/GPG-44?atlOrigin=eyJpIjoiNTJhN2E3NWJjNGRlNDk1ZGEyODk0N2QxNDAxNjQ0OWUiLCJwIjoiaiJ9): Created the main loop for the snake game. Commits: [Main Loop](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/afe984d6561e9836d8618d80ed6b1cde3442eeec)
* [GPG-35](https://cs3398s22gorns.atlassian.net/browse/GPG-35?atlOrigin=eyJpIjoiMzhiNDNiNjA3M2M2NDAxNTgxNzkzZmMxMGEwNGM3ZjgiLCJwIjoiaiJ9): Coded the necessary classes for the snake game. Major commits: [Input Handling](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/a303d5b47cdc008ceb25401da8c8de14ae42923f), [Made Snake Segment](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/1637c97ed80e898fc0b9358dfb194d6b2a970216), [Restructured Game](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/6aae29324c0e4e71750d437a00bf16d48eef5067), [Reimplemented Lost Functionality](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/d577745881de91f46fe804687f29eab04ff8d208), [Movement](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/74eb88cd9ac653d523f5b86883b11eca7122a3e9), [Wall Collision](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/b7c756eb942e2b14e43a8e65d8765801fc9fcd17), [Snake Collision](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/b6ea3e2234a94f51a2a63220d4a7a48880a6d0d0), [Apple Collision](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/6649eedb1a6ece9895e54815a9a742bb30c5e5ba), [Collision Bugfix](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/6d03990e3e655ca29f78226b66cc71b3f89d90ad), [Game Over](https://bitbucket.org/cs3398-s22-gorns/gorns_pool_game/commits/fd1b7cd9df3e0a6e748f1c71cd3ccbd33419b775)

### Andrew Contribution

## Sprint 3 Retrospective

### What Went Well?

***Team:***

* The four games we created all work as intended.
* All the games we created integrated into the website smoothly.
* We communicated well and started to work even more smoothly as a team.
* We were able to help each other with our weak points, rounding out the team and letting us function even better than a sum of the individuals.

***Individual:***

* Andrew - 

* Ryan - Using html, JavaScript, and CSS together went well when creating the snake game. Originally expected it to be very difficult to get the three code document types to work together, but it went well and was very educational.

* Eastland - Creating individual games and porting them onto the live website went way smoother than expected. 

### What Might Be Impeding Us from Performing Better?

* Team - Understanding how the multiple platforms interact with each other. (I.E: GitKraken, Jira, Bitbucket.) 

### What Can I Do To Improve?

* Andrew - 

* Ryan - Fully understand how a coding language works for the purpose you want to use it for before using it. I had the snake game 80% finished, and I realized that I needed to structure everything completely differently, so I had to rework a lot of stuff that I had already completed. Knowing how JavaScript and CSS interacted beforehand could have prevented this from happening.

* Eastland - Measure/manage my time better. Design more thoroughly before coding. 

## Next Steps

* Improve the website style/look to be more clean.
* Add more minor updates to game. (I.E Scoreboards, etc)
* Add more games to site.