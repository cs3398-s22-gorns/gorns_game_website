export let keysPressed = {up: false, down: false, left: false, right: false};
export let lastKeysPressed = {up: false, down: false, left: false, right: false};

export function setListeners() {

    document.addEventListener('keydown', (event) => {
        if(event.code == 'ArrowUp') {
            keysPressed.up = true;
        }
        if(event.code == 'ArrowDown') {
            keysPressed.down = true;
        }
        if(event.code == 'ArrowLeft') {
            keysPressed.left = true;
        }
        if(event.code == 'ArrowRight') {
            keysPressed.right = true
        }
    })

    document.addEventListener('keyup', (event) => {
        if(event.code == 'ArrowUp') {
            keysPressed.up = false;
        }
        if(event.code == 'ArrowDown') {
            keysPressed.down = false;
        }
        if(event.code == 'ArrowLeft') {
            keysPressed.left = false;
        }
        if(event.code == 'ArrowRight') {
            keysPressed.right = false;
        }
    })
}

export function setLastKeysPressed() {

    if(keysPressed.up || keysPressed.down || keysPressed.left || keysPressed.right) {

        lastKeysPressed.up = keysPressed.up;
        lastKeysPressed.down = keysPressed.down;
        lastKeysPressed.left = keysPressed.left;
        lastKeysPressed.right = keysPressed.right;

        if(keysPressed.up && keysPressed.down) {
            lastKeysPressed.down = false;
        }
        if(keysPressed.left && keysPressed.right) {
            lastKeysPressed.left = false;
        }
    }
}