export default class Apple {

    constructor(appleElem) {

        this.appleElem = appleElem;
    }

    relocate() {

        this.appleElem.style.setProperty("--xPos", this.generateRandom(-15, 16));
        this.appleElem.style.setProperty("--yPos", this.generateRandom(-15, 16));
    }

    generateRandom(max, min) {

        return Math.floor(Math.random() * (max - min) + min);
    }

    get x() {

        return parseFloat(getComputedStyle(this.appleElem).getPropertyValue("--xPos"));
    }

    get y() {

        return parseFloat(getComputedStyle(this.appleElem).getPropertyValue("--yPos"));
    }
}