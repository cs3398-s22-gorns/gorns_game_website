import Snake from './Snake.js'
import Apple from './Apple.js';
import { lastKeysPressed, setListeners, setLastKeysPressed } from './Input.js';
import { handleSnakeCollision, handleWallCollision } from './HandleCollision.js';

setListeners();

const snake = new Snake(document.getElementById('player'));
const apple = new Apple(document.getElementById('fruit'));
let gameOver = false;

apple.relocate();

function mainLoop() {

    setLastKeysPressed();

    if(willEatApple()) {
        snake.grow();
        apple.relocate();
    }

    snake.move(lastKeysPressed);

    gameOver = handleSnakeCollision(snake);
    if(!gameOver) {
        gameOver = handleWallCollision(snake);
    }

    if(!gameOver) {
        setTimeout(() => {
            window.requestAnimationFrame(mainLoop)
        }, 100);
    } else {
        let newDiv = document.createElement("div");
        newDiv.className = "gameOverBox";
        let newP = document.createElement("p")
        newP.textContent = "Game Over!";
        newP.className = "gameOverText";
        newDiv.appendChild(newP);
        document.body.appendChild(newDiv);
    }
}

function willEatApple() {

    let newX = snake.x;
    let newY = snake.y;

    if(lastKeysPressed.up) {
        newY--;
    }
    if(lastKeysPressed.down) {
        newY++;
    }
    if(lastKeysPressed.left) {
        newX--;
    }
    if(lastKeysPressed.right) {
        newX++;
    }

    if(newX == apple.x && newY == apple.y) {
        return true;
    } else {
        return false;
    }
}

window.requestAnimationFrame(mainLoop);