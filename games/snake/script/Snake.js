export default class Snake {

    constructor(snakeElem) {

        this.snakeElem = snakeElem;
        this.nextSegment = null;
        this.prevSegment = null;
    }

    set previous(prev) {

        this.prevSegment = prev;
    }

    get next() {

        return this.nextSegment;
    }

    get x() {

        return parseFloat(getComputedStyle(this.snakeElem).getPropertyValue("--xPos"));
    }

    set x(newX) {

        this.snakeElem.style.setProperty("--xPos", newX);
    }

    get y() {

        return parseFloat(getComputedStyle(this.snakeElem).getPropertyValue("--yPos"));
    }

    set y(newY) {

        this.snakeElem.style.setProperty("--yPos", newY);
    }

    grow() {

        let newDiv = document.createElement("div");
        newDiv.className = "snake";
        document.body.appendChild(newDiv);
        let newSegment = new Snake(newDiv);

        let tail = this;
        while(tail.next != null) {
            tail = tail.next;
        }

        tail.nextSegment = newSegment;
        tail.nextSegment.previous = tail;
    }

    move(directions) {

        if(this.nextSegment != null) {
            this.nextSegment.follow();
        }
        if(directions.up) {
            this.y -= 1;
        }
        if(directions.down) {
            this.y += 1;
        }
        if(directions.left) {
            this.x -= 1;
        }
        if(directions.right) {
            this.x += 1;
        }
    }

    follow() {

        if(this.nextSegment != null) {
            this.nextSegment.follow();
        }
        this.x = this.prevSegment.x;
        this.y = this.prevSegment.y;
    }
}