export function handleSnakeCollision(snake) {

    let bodySeg = snake;

    while(bodySeg.next != null) {

        bodySeg = bodySeg.next;
        
        if(snake.x == bodySeg.x && snake.y == bodySeg.y) {
            console.log("Game Over");
            return true;
        }
    }

    return false;
}

export function handleWallCollision(snake) {

    if(snake.x > 15 || snake.x < -15 || snake.y > 15 || snake.y < -15) {
        return true;
    } else {
        return false;
    }
}