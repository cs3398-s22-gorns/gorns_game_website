// Timer

var timer;
var ele = document.getElementById ('timer');

(function (){
  var sec = 0;
  timer = setInterval(() => {
    ele.innerHTML = 'Score:'+sec;
    sec ++;
  }, 1000) // 1 sec
  
})()

const gorn = document.getElementById("gorn");
const rock = document.getElementById("rock");

function jump() {
  if (gorn.classList != "jump") {
    gorn.classList.add("jump");

    setTimeout(function () {
      gorn.classList.remove("jump");
    }, 300);
  }
}

let isAlive = setInterval(function () {
  
  let gornTop = parseInt(window.getComputedStyle(gorn).getPropertyValue("top"));

  
  let rockLeft = parseInt(
    window.getComputedStyle(rock).getPropertyValue("left")
  );

  if (rockLeft < 25 && rockLeft > 0 && gornTop >= 140) {
    // collision
    alert("Game Over!");
    function reset(){
      clearInterval(timer);
    }
    window.location.reload(true);
    
  }
}, 10);

document.addEventListener("keydown", function (event) {
  jump();
});

