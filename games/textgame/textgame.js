const textElement = document.getElementById('text')
const optionButtonsElement = document.getElementById('option-buttons')

let state = {}

function startGame() {
  state = {}
  showTextNode(1)
}

function showTextNode(textNodeIndex) {
  const textNode = textNodes.find(textNode => textNode.id === textNodeIndex)
  textElement.innerText = textNode.text
  while (optionButtonsElement.firstChild) {
    optionButtonsElement.removeChild(optionButtonsElement.firstChild)
  }

  textNode.options.forEach(option => {
    if (showOption(option)) {
      const button = document.createElement('button')
      button.innerText = option.text
      button.classList.add('btn')
      button.addEventListener('click', () => selectOption(option))
      optionButtonsElement.appendChild(button)
    }
  })
}

function showOption(option) {
  return option.requiredState == null || option.requiredState(state)
}

function selectOption(option) {
  const nextTextNodeId = option.nextText
  if (nextTextNodeId <= 0) {
    return startGame()
  }
  state = Object.assign(state, option.setState)
  showTextNode(nextTextNodeId)
}

const textNodes = [
  {
    id: 1,
    text: 'You awaken in a cell. You have a terrible headache from doing too many death sticks and drinking too much blue milk last night... Suddenly an alarm goes off and flashing redlights appear outside of your cell door. The automated bars slide open to a hall way of other cells being opened with prisoners stepping out. Which way do you go?',
    options: [
      {
        text: 'Take a left.',
        nextText: 2
      },
      {
        text: 'Take a right.',
        nextText: 4
      }
    ]
  },
  {
    id: 2,
    text: 'You turn left and push your way through the crowd of prisoners who are all going in the opposite direction. You peek your head up and see a terrifying group of green lizard monsters making their way towards you. Biting and slashing their way through the fleeing prisoners. Do you continue forth or turn around?',
    options: [
      {
        text: 'Continue forward!',

        nextText: 3
      },
      {
        text: 'Turn around and run away!!!',

        nextText: 4
      },
    ]
  },
  {
    id: 3,
    text: 'You decide the group of lizard monsters are no threat and continue on your way towards them. Once you come within feet of them with no other prisoners nearby you stop in your tracks. Putting your fist up to fight them in a good ol fashioned fist fight. The monsters turn their heads and look at each other confused. They gurgle and then all lung at you... tearing you apart limb from limb... ',
    options: [
      {
        text: 'Restart',
        nextText: -1
      }
    ]
  },
  {
    id: 4,
    text: 'You move along with the other prisoners making your way down the hall way, hearing people screaming and monsters roaring. At the end of the hall you see a group of prisoners urging you to hurry up before the security door fully closes! You pick up the pace with the other prisoners and just narroly make it through as the automated door shuts behind you. You turn to look through the glass panel and see one of the lizard monsters try to claw their way through. You recognize this species of monster. It is a Gorn!   You take a few steps back and bump into the group of prisoners yelling and bickering about the situation. You look around and find a holo-map on one of the walls, you go to examine it. Med-Bay: 0103  Armory:2124  Escape-Pods:4534  Captains-Deck: 9453  You try and commit the rooms and numbers to memory. A loud bang echos through out the room, the security door is being broken down by the gorns! The crowd flees, some going to the upper level of 4000 and some going to the lower level of 2000.   Which level do you go to?',
    options: [
      {
        text: 'Go up to level 4000!',
        nextText: 5
      },
      {
        text: 'Go down to level 2000!',
        nextText: 6
      }
    ]
  },
  {
    id: 5,
    text: 'You charge up the stairs passing others by and making your way up to level 4000. A large holo-sign reads "EMERGENCY PODS: 14/100" The ships crew, prisoners, and all other personal have taken most of the pods. You might not be able to secure a pod. Do you stay and try and find a pod or do you continue on up to the higher level?',
    options: [
      {
        text: 'Try to secure an escape pod!',
        nextText: 8
      },
      {
        text: 'Continue going to the higher levels, prehaps we can figure something else out!',
        nextText: 9
      }
    ]
  },
  {
    id: 6,
    text: 'You make your way down to the lower level! You hear the gorns making their way down not too far away from you! You see a holo-sign stating: <-:2000 - 2499  ||  2500-2700:->  Which way do you go? ',
    options: [
      {
        text: 'Go Left! 2000-2499',
        nextText: 7
      },
      {
        text: 'Go Right! 2500-2700',
        nextText: 10
      }
    ]
  },
  {
    id: 7,
    text: 'You take a left and after a few seconds running down the hall you find the armory! You see others have already armed themselves and hand you a pulse rifle. Hearing the gorns making their way towards the room everyone takes defensive postions. The storm in and a barrage of laser beams go flying. Some gorns go down but the majority make their way in and start tearing everyone up. You and one other are the last ones standing before the horde fully make their way to you two and devour you whole. RIP! ',
    options: [
      {
        text: 'Restart',
        nextText: -1
      }

    ]
  },
  {
    id: 8,
    text: 'You push your way past the crowd. You see escape-pods launching left and right, but you see one at the end of the hall; you rush to it! You wait for the pod doors to open up as gorns make their way down the hall, making their way to you. The pod doors finally opem, you jump in and punch in random coordinates! The pods engine revs up and launch you out of the ship! You escaped the threat of the gorns but now find yourself on a journey with no known destination into the infinite abyss of space...',
    options: [
      {
        text: 'You escaped! Good Job! ',
        nextText: -1
      }
    ]
  },
  {
    id: 9,
    text: 'You make your way up to the higher levels, finding your self on the captains level. Guards armed with pule rifles urge you to move past them as they hold their position in preparation for the gorns rushing up the stairs. You find your way to the captains deck and see the legend himself... JOHN ALIEN. John Alien walks over to you and welcomes you to stay up here and hold out along side with him and his crew. Being given a laser sword you and the others brace yourselfs for battle. The gorns make their way through the guards and now storm the deck. You fend them off with your laser sword while fighting along side John Alien. You slice through gorn after gorn but they still keep coming... you start to tire out and the gorns overpower you and John Alien, thus ending your story...',
    options: [
      {
        text: 'You died along side John Alien. Restart?',
        nextText: -1
      }
    ]
  },
  {
    id: 10,
    text: 'You jog down the hall not finding anything of importance. You see gorns catching up from behind. You open a random door and lock yourself into a room, taking a few steps back and holding your breathe you hear the gorns getting closer... Until you hear a loud thud! The door is torn off and a swarm of gorns charge into the room straight at you. You are torn to bits and eaten as a tasty gorn snack. RIP.',
    options: [
      {
        text: 'Restart',
        nextText: -1
      }
    ]
  },
  
]

startGame()