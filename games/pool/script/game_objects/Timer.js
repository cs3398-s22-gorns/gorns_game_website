"use strict";

function Timer(position) {
    this.position = position;
    this.origin = new Vector2(0, 0);
    this.hour = 0;
    this.minute = 0;
    this.second = 0;
    this.startTime = new Date().getTime();
}

Timer.prototype.reset = function() {
    this.hour = 0;
    this.minute = 0;
    this.second = 0;
    this.startTime = new Date().getTime() - 5000;
};

Timer.prototype.draw = function() {
    Canvas2D.drawText(
        ("0" + this.hour).slice(-2) + ":" + ("0" + this.minute).slice(-2) + ":" + ("0" + this.second).slice(-2),
        this.position,
        this.origin,
        "#096834", 
        "top", 
        "Impact", 
        "120px"
    );
};

Timer.prototype.updateTime = function() {
    let currentTime = new Date().getTime();
    let timeDiff = currentTime - this.startTime - 5000;
    this.hour = Math.floor(timeDiff / (1000 * 60 * 60));
    this.minute = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));
    this.second = Math.floor((timeDiff % (1000 * 60)) / 1000);
};